package com.goostree.codewars;

import java.util.List;

public class PaginationHelper<I> {
	
	/**
	   * The constructor takes in an array of items and a integer indicating how many
	   * items fit within a single page
	   */
	  public PaginationHelper(List<I> collection, int itemsPerPage) {
	     this.collection = collection;
	     this.itemsPerPage = itemsPerPage;
	  }
	  
	  /**
	   * returns the number of items within the entire collection
	   */
	  public int itemCount() {
		  return collection.size();
	  }
	  
	  /**
	   * returns the number of pages
	   */
	  public int pageCount() {
		  return (int) Math.ceil(((double)itemCount())/itemsPerPage);
	  }
	  
	  /**
	   * returns the number of items on the current page. page_index is zero based.
	   * this method should return -1 for pageIndex values that are out of range
	   */
	  public int pageItemCount(int pageIndex) {
		  int itemCount = 0;
		  
		  if(((0 <= pageIndex) && (pageIndex < pageCount())) && itemCount() != 0) {
			  for(int i = 0; i < itemCount(); i++) {
				  if(i / itemsPerPage == pageIndex) {
					  itemCount++;
				  }
			  }
		  } else {
			  itemCount = -1;
		  }
		  
		  return itemCount;
	  }
	  
	  /**
	   * determines what page an item is on. Zero based indexes
	   * this method should return -1 for itemIndex values that are out of range
	   */
	  public int pageIndex(int itemIndex) {
		  int pageIndex = 0;
		  
		  if(((0 <= itemIndex) && (itemIndex < itemCount())) && itemCount() != 0) {
			  pageIndex = itemIndex / itemsPerPage;
		  } else {
			  pageIndex = -1;
		  }
		  
		  return pageIndex;
	  }
	  
	  private List<I> collection;
	  private int itemsPerPage;
}
