package com.goostree.codewars.test;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.runners.JUnit4;

import com.goostree.codewars.PaginationHelper;

public class SolutionTest {
	@Test
    public void testSomething() {
        // assertEquals("expected", "actual");
		PaginationHelper<Character> helper = 
				new PaginationHelper<Character>(Arrays.asList('a', 'b', 'c', 'd', 'e', 'f'), 4);
		
		assertEquals(helper.pageCount(), 2);
		assertEquals(helper.itemCount(), 6);
		
		assertEquals(helper.pageItemCount(0), 4);
		assertEquals(helper.pageItemCount(1), 2);
		assertEquals(helper.pageItemCount(2), -1);
		
		assertEquals(helper.pageIndex(5), 1);
		assertEquals(helper.pageIndex(2), 0);
		assertEquals(helper.pageIndex(20), -1);
		assertEquals(helper.pageIndex(-10), -1);
    }	
}
